## microservices-simple-product

**Spring4 + maven + spring boot **

Proyecto prueba de microservicio REST con Spring Boot

Para ejecutar aplicacion:

`mvn spring-boot:run`

Ejemplo ejecucion en cliente:

`curl -v -X GET http://localhost:9000/api/products`

Enlaces de interes:

    spring-boot-starter-data-rest --> http://docs.spring.io/spring-data/rest/docs/current/reference/html/
                                      https://spring.io/guides/gs/accessing-data-rest/

    spring-boot-starter-data-jpa --> https://spring.io/guides/gs/accessing-data-jpa/

